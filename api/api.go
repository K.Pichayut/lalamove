package api

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/pichayut/lalamove/consts"
	"github.com/pichayut/lalamove/models"
	"github.com/pichayut/lalamove/repository"
	"github.com/pichayut/lalamove/util/config"
)

type Env struct {
	Repo   repository.Repo
	Config config.Config
}

func (env *Env) GetOrderDetails(c *gin.Context) {

	var (
		OrderDetails    = c.Param("id")
		response        models.OrderDetailsResponse
		assigningDriver = true
	)

	fmt.Println(OrderDetails)

	// ASSIGNING_DRIVER

	if assigningDriver == true {
		response.Status = consts.Desc1
		response.Price.Amount = 108000
		response.Price.Currency = consts.CurrencTH
		response.DriverID = ""
	}

	//ASSIGNING_DRIVER

	//PICKED_UP

	//COMPLETED

	//CANCELED

	//REJECTED

	//EXPIRED

	c.JSON(http.StatusOK, response)
}
