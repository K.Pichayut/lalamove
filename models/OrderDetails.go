package models

//response

type OrderDetailsResponse struct {
	Status   string `json:"status"`
	Price    PriceResult
	DriverID string `json:"driveId"`
}

type PriceResult struct {
	Amount   int    `json:"amount"`
	Currency string `json:"currency"`
}
