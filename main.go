package main

import (
	"fmt"

	"github.com/pichayut/lalamove/api"
	"github.com/pichayut/lalamove/repository"
	"github.com/pichayut/lalamove/route"
	"github.com/pichayut/lalamove/util/config"
)

func main() {
	//CONFIGURATION
	conf := config.Load()

	//INITLIZE DATABASE
	db, err := repository.Init("mongodb://" + conf.MongoAuth + conf.MongoURL + ":" + conf.MongoPort)
	if err != nil {
		fmt.Println(err.Error())
	}

	//-----------API----------------
	//SETUP ROUTER
	r := route.Env{api.Env{Repo: db, Config: conf}}.Router()

	r.Run(":" + conf.AppPort)

}
