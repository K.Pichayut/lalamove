package consts

// const code
const (
	Desc1 = "ASSIGNING_DRIVER"
	Desc2 = "ON_GOING"
	Desc3 = "PICKED_UP"
	Desc4 = "COMPLETED"
	Desc5 = "CANCELED"
	Desc6 = "REJECTED"
	Desc7 = "EXPIRED"

	CurrencTH = "THB"
)
