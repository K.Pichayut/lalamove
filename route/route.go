package route

import (
	"github.com/gin-gonic/gin"
	"github.com/pichayut/lalamove/api"
)

type Env struct {
	api.Env
}

//Router is setup routing
func (e Env) Router() *gin.Engine {
	r := gin.Default()

	r.GET("/v2/orders/:id", e.GetOrderDetails)

	return r
}
