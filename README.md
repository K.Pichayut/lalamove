RUN : go run main.go

Port: 9080

Order Details

GET /v2/orders/{id} 
id	<LALAMOVE_ORDER_ID>

example localhost:9080/v2/orders/1223

Expect Response 
example 

{
    "status": "ASSIGNING_DRIVER",
    "Price": {
        "amount": 108000,
        "currency": "THB"
    },
    "driveId": ""
}