package repository

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

//DB initialize
type DB struct {
	*mongo.Client
}

//Init initialize
func Init(dataSourceName string) (*DB, error) {
	clientOptions := options.Client().ApplyURI(dataSourceName)

	db, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		return nil, err
	}
	if err = db.Ping(context.TODO(), nil); err != nil {
		return nil, err
	}
	return &DB{db}, nil
}
